#Comentarios Varios y diversos.
#
#Autores: Naiara Garcia, Edgar Benedico
#
#Descripcion: Busca archivos comida en todas las carpetas que puede entrar.
#	Va perdiendo vida conforme avanza y recupera cuando come.
#	Si se queda sin vida muere. Si no, se va replicando a cada carpeta que entra.
#
#Info ejecutable
#sudo bash ant.sh numeroDeVidas extensionDeArchivosComida nombreArchivoSemilla rutaDeEjecucionDelSH
#
#Ejemplo:
#sudo bash ant.sh 100 ant x `pwd`


# 1 Variables locales
let gastoVidaCarpeta=5
let gastoVidaBuscarArchivo=1
let vidaPorArchivoEncontrado=1
nombreArchivo="ant.sh"

# 2 Recibe como parametros: Vida, Objetivos, Semilla.
let vida=$1
objetivo=$2
semilla=$3
root=$4

echo Acabamos de iniciar en $root con una vida de $vida
echo --------------------------------------------------

# 3 Se resta la cantidad de puntos de vida por entrar en una carpeta.
let vida=$vida-$gastoVidaCarpeta

echo Al entrar en la carpeta hemos perdido 5 de vida, por lo tanto nos quedamos con $vida
echo --------------------------------------------------

# 4 Si su vida > 0, prosigue. Si no genera un archivo Rip.ant y para.
if [ $vida -le 0 ];
then
	touch "$root/Rip.ant"
	#rm "$root/$nombreArchivo"

	echo Nos hemos muerto
	echo --------------------------------------------------

	exit 0
fi

# 5 Busca en a carpeta si hay archivos del tipo Objetivo.
#	Si encuentra, calcula la vida recuperada.
#	Recupera vida y finalmente elimina el archivo objetivo.

echo Seguimos vivos
echo --------------------------------------------------

echo Vamos a buscar comida

for elemento in `ls $root`
do
	if [ ${elemento#*.} == $objetivo ];
	then
		echo Hemos encontrado comida, subimos vida
		let vida=$vida+$vidaPorArchivoEncontrado
		rm "$root/$elemento"
	fi
done

echo Hemos acabado de comer, tenemos $vida de vida
echo --------------------------------------------------


# 6 Cuenta los archivos que quedan en la carpeta. Calcula la vida perdida
#	y finalmente pierde vida.

echo Hemos seguido buscando comida

for elemento in `ls $root`
do
	if [ ! -d $elemento ];
	then
		echo Nos cansamos por encontrar cosas que no podemos comer, perdemos vida
		let vida=$vida-$gastoVidaBuscarArchivo
	fi
done

echo Hemos acabado de buscar, tenemos $vida de vida
echo --------------------------------------------------

# 7 Repite #4

if [ $vida -le 0 ];
then
	touch "$root/Rip.ant"
	#rm "$root/$nombreArchivo"

	echo Nos hemos muerto
	echo --------------------------------------------------

	exit 0
fi

echo Seguimos vivos
echo --------------------------------------------------

# 8 Busca la primera carpeta que no contenga una semilla.

echo Buscamos carpetas donde no hayamos pasado antes

for elemento in `ls $root`
do
	if [ -d $elemento ];
	then
		tieneSemilla=false

		echo Hemos encontrado $root/$elemento

		for elementos in `ls "$root/$elemento"`
		do

			if [ $elementos == $semilla ];
			then
				echo Por aqui ya hemos pasado
				tieneSemilla=true
			fi

		done

		if [ $tieneSemilla == false ] ;
		then
			echo Por aqui no hemos pasado. Asi que vamos a entrar con $vida de vida

			mv "$root/$nombreArchivo" "$root/$elemento/"
			bash "$root/$elemento/$nombreArchivo" $vida $objetivo $semilla "$root/$elemento" &
			exit 0
		fi

	fi

done

echo No hemos encontrado ninguna carpeta nueva
echo --------------------------------------------------


# 9 Si no encuentra, deja una semilla en el directorio.
#	Se desplaza a la carpeta madre.
#	Ejecuta el nuevo programa con los argumentos correspondientes
#	en modo oculto y finalmente para


echo Vamos a dejar una semilla en $root y nos volveremos a ${root%*/*}

touch "$root/$semilla"

mv "$root/$nombreArchivo" "${root%*/*}/"

bash "${root%*/*}/$nombreArchivo" $vida $objetivo $semilla ${root%*/*} &

exit 0
